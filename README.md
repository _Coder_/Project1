# Document Manger and User Interface

Developed using C and uses structures, string manipulation, file I/O and text parsing.

•	It allows the user to add paragraphs, lines to paragraphs, replace, remove and highlight text.
•	The user interface adds extra functionality to the manger file.
