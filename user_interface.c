
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sysexits.h> 
#include <ctype.h> 
#include <errno.h>
#include "document.h"

int commands(Document *doc, char *line){ 
  char name[MAX_STR_SIZE+1] = {0} ;
  char check[MAX_STR_SIZE+1] = {0} ;
  char num[MAX_STR_SIZE+1] = {0} ;
  char line_read[MAX_STR_SIZE + 1] = {0} ; 
  char new_l[MAX_STR_SIZE+1] = {0};
  char target[MAX_STR_SIZE +1] = {0} ;
  char replacement[MAX_STR_SIZE + 1] = {0} ; 
  char target_without_quotes[MAX_STR_SIZE + 1] = {0}; 
  char replacement_without_quotes[MAX_STR_SIZE + 1] = {0} ; 
  int para_num = 0, read = 0 , line_num = 0,
    offset =0 , i = 0, j = 0, found = 0, count = 0;  
	     
  for(i = 0; i < strlen(line); i++){ 
    if(isspace(line[i])) 
      count++; 
    else 
      break; 
  } 
  sscanf(line, "%s", name);     
  if(count != strlen(line)){ 
    if(strcmp(line, "\n") != 0 && strcmp(line, "\0") && name[0] != '#'){ 
      /* add_paragraph_after command.*/ 
	      if(strcmp(name, "add_paragraph_after") == 0){ 
        read = sscanf(line, "%s%s%s", name, num, check); 
        para_num = atoi(num); 
         
        if((strcmp(num, "0") != 0 && para_num <= 0) || read != 2){ 
          printf("Invalid Command\n"); 
        } 
        /*If the command cannot be successfully excuted.*/ 
        else if(add_paragraph_after(doc, para_num) == FAILURE) { 
          printf("add_paragraph_after failed\n"); 
        } 
      } 
       
      /* add_line_after command */ 
      else if(strcmp(name, "add_line_after") == 0){       
        read = sscanf(line, "%s%s%s%n", name, num, line_read, &offset); 
        para_num = atoi(num); 
        line_num = atoi(line_read); 
	         
        for(i = offset, j = 0; i < strlen(line) && line[i] != '\n'; i++) { 
          /* Checks for the * */ 
          if(!found && line[i] == '*') { 
            found = 1; 
            continue;           
          } 
          if(found) { 
            new_l[j] = line[i]; 
            j++; 
          } 
        } 
        new_l[j] = '\0'; 
        /* If conditions are invalid.*/ 
       if(!found || read != 3) { 
          printf("Invalid Command\n"); 
        } else if((strcmp(num, "0") != 0 && para_num <= 0) 
                  || (strcmp(line_read, "0") != 0 && line_num <= 0)) { 
          printf("Invalid Command\n"); 
        }        
        /*If the command cannot be successfully excuted.*/ 
        else if(add_line_after(doc, para_num,
			       line_num, new_l) == FAILURE){ 
          printf("add_line_after failed\n"); 
        } 
      } 
       
      /* print_document command*/ 
      else if(strcmp(name, "print_document") == 0) { 
        read = sscanf(line, "%s%s", name, check); 
         
        if(read == 1) { 
          /*Prints the document.*/ 
          print_document(doc);    
        } else { 
          printf("Invalid Command\n"); 
        } 
      } 
       
      /* Quit command*/    
      else if (strcmp(name, "quit") == 0){ 
        read = sscanf(line, "%s%s", name, check); 
	         
        if(read != 1) { 
          printf("Invalid Command\n"); 
        } else { 
          return EXIT_SUCCESS; 
        } 
      } 
	   
      /*Exit command*/ 
      else if (strcmp(name, "exit") == 0){ 
        read = sscanf(line, "%s%s", name, check); 
	           
        if(read != 1) { 
          printf("Invalid Command\n"); 
        } else { 
         return EXIT_SUCCESS; 
        } 
      } 
	   
      /*append_line command*/ 
      else if(strcmp(name, "append_line") == 0){       
        read = sscanf(line, "%s%s%n", name, num, &offset); 
        para_num = atoi(num); 
	         
        for(i = offset, j = 0; i < strlen(line) && line[i] != '\n'; i++) { 
          /* Checks for * */ 
         if(!found && line[i] == '*') { 
            found = 1; 
            continue;           
          } 
          if(found) { 
            new_l[j] = line[i]; 
            j++; 
          } 
        } 
        new_l[j] = '\0'; 
        /* Invalid conditions*/ 
        if(!found || read != 2) { 
          printf("Invalid Command\n"); 
        } else if(strcmp(num, "0") != 0 && para_num <= 0) { 
          printf("Invalid Command\n"); 
        } else if (append_line(doc, para_num, new_l) == FAILURE) { 
          printf("append_line failed\n"); 
        } 
      } 
	 
      /* remove_line command*/ 
      else if(strcmp(name, "remove_line") == 0){       
        read = sscanf(line, "%s%s%s%n", name, num, line_read, &offset); 
        para_num = atoi(num); 
        line_num = atoi(line_read);         
        
        if(read != 3) { 
          printf("Invalid Command\n"); 
        } else if((strcmp(num, "0") != 0 && para_num <= 0) 
                  || (strcmp(line_read, "0") != 0 && line_num <= 0)) { 
          printf("Invalid Command\n"); 
	        }         
        /*If the command cannot be successfully excuted.*/ 
        else if(remove_line(doc, para_num, line_num) == FAILURE){ 
         printf("remove_line failed\n"); 
        } 
     } 
 
      /* remove_line command*/ 
      else if(strcmp(name, "load_file") == 0){       
        read = sscanf(line, "%s%s%n", name, line_read, &offset); 
	         
        if(read != 2) { 
          printf("Invalid Command\n"); 
        }         
        /*If the command cannot be successfully excuted.*/ 
        else if(load_file(doc, line_read) == FAILURE) { 
          printf("load_file failed\n"); 
        }         
      } 
	 
      /* replace_text command*/ 
      else if(strcmp(name, "replace_text") == 0){ 
        read = sscanf(line, "%s%s%n", name, target, &offset); 
	 
        for(i = 0, j = 0; i < strlen(target); i++) { 
          if(target[i] != '"') { 
            target_without_quotes[j++] = target[i]; 
          } 
        }         
        found = 0; 
        for(i = offset, j = 0; i < strlen(line); i++) { 
          /* When the target is found.*/ 
          if(!found && line[i] == '"') { 
            found = 1;    
          } else if(found) { 
            if(line[i] == '"') {  
              replacement_without_quotes[j++] = '\0'; 
              break; 
          } 
           replacement_without_quotes[j++] = line[i]; 
          } 
        }            
        if(read != 2 || strlen(replacement_without_quotes) == 0) { 
          printf("Invalid Command\n"); 
        }         
        /*If the command cannot be successfully excuted.*/ 
        else if(replace_text(doc, target_without_quotes,
			     replacement_without_quotes) == FAILURE) { 
          printf("replace text failed\n"); 
        } 
      } 
 
      /* highlight_text command */ 
      else if(strcmp(name, "highlight_text") == 0){       
        read = sscanf(line, "%s%n", name, &offset); 
	 
        found = 0; 
        for(i = offset, j = 0; i < strlen(line); i++) { 
          if(!found && line[i] == '"') { 
            found = 1; 
            continue;           
          } else if(found) { 
            if(line[i] == '"') { 
              target_without_quotes[j++] = '\0'; 
              break; 
            } 
            target_without_quotes[j++] = line[i]; 
          } 
        }      
        if(read != 1 /*|| strlen(target) == 0*/) { 
          printf("Invalid Command\n"); 
        } 
        highlight_text(doc, target_without_quotes); 
      } 
	 
      /* remove_text command */ 
      else if(strcmp(name, "remove_text") == 0){       
        read = sscanf(line, "%s%n", name, &offset); 
        found = 0; 
	         
        for(i = offset, j = 0; i < strlen(line); i++) { 
          if(!found && line[i] == '"') { 
            found = 1; 
            continue;           
          } else if(found) { 
            if(line[i] == '"') { 
              target_without_quotes[j++] = '\0'; 
              break; 
            } 
            target_without_quotes[j++] = line[i]; 
          } 
        }         
        if(read != 1 || strlen(target_without_quotes) == 0) { 
          printf("Invalid Command\n"); 
        } 
        remove_text(doc, target_without_quotes); 
      } 
 
      /* save_document command*/ 
      else if(strcmp(name, "save_document") == 0){       
        read = sscanf(line, "%s%s%n", name, line_read, &offset); 
	         
        if(read != 2) { 
          printf("Invalid Command\n"); 
        }         
        /*If the command cannot be successfully excuted.*/ 
        else if(save_document(doc, line_read) == FAILURE) { 
          printf("save document failed\n"); 
        } 
      } 
	 
      /* Reset Document*/ 
      else if(strcmp(name, "reset_document") == 0){ 
        read = sscanf(line, "%s %s", name, check); 
 
          if (read == 1) { 
            reset_document(doc); 
          } 
          else { 
            printf("Invalid Command\n"); 
          }   
        } 
     
      else  { 
          printf("Invalid Command\n"); 
        } 
    } 
  } 
  return 1; 
} 
	 
      
int main(int argc, char *argv[]){ 
  FILE *input; 
  Document doc; 
  char string_line[MAX_STR_SIZE+1] = {0}; 
  int return_value = 0; 

  
  /* Checks for the number of arguments.*/ 
  if(argc == 1){ 
      input = stdin; 
    /* Takes in standard input*/     
  } else if(argc == 2) { 
    /* Uses the file the user gives.*/ 
    input = fopen(argv[1], "r"); 
    if(input == NULL){ 
      fprintf(stderr, "%s cannot be opened\n.", argv[1]); 
      return EX_OSERR; 
    } 
  } else if(argc > 2) { 
    fprintf(stderr, "Usage: user_interface\n"); 
    fprintf(stderr, "Usage: user_interface <filename>\n"); 
    return EX_USAGE; 
  } 
	 
  /* intializing the document */ 
  init_document(&doc, "main_document"); 
	 
	 
  /* Looping until the end of file is reached. */ 
  while(!feof(input)){ 
    if(argc == 1) { 
      printf("> "); 
    } 
    fgets(string_line, MAX_STR_SIZE+1, input);     
    return_value = commands(&doc, string_line); 
    if(return_value == EXIT_SUCCESS) { 
      break; 
    } 
  } 
  fclose(input); 
  return EXIT_SUCCESS; 
} 
