
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "document.h"

/* Initializes a new document which is empty.*/
/* An empty document has no paragraphs.*/
int init_document(Document *doc, const char *name){
  
  if(doc == NULL || name == NULL || strlen(name) > MAX_STR_SIZE){
    return FAILURE;
  }
  doc->number_of_paragraphs = 0;
  strcpy(doc->name, name);
  return SUCCESS;
}

/*The function sets number of para to zero.*/
int reset_document(Document *doc){
  
  if(doc == NULL){
    return FAILURE;
  }  
  doc->number_of_paragraphs = 0;
  return SUCCESS;
}

/*The function prints the document along with the document name.*/
/*number of pargraphs and then the paragraphs.*/
int print_document(Document *doc){
  int i = 0, j = 0;
  
  if(doc == NULL){
    return FAILURE;
  }
  
  printf("Document name: \"%s\"\n", doc->name);
  printf("Number of Paragraphs: %d\n", doc->number_of_paragraphs);
  /*Goes over each paragraph*/
  while(i < doc->number_of_paragraphs){
    if(doc->paragraphs[i].number_of_lines > 0) {
      for(j = 0; j < doc->paragraphs[i].number_of_lines; j++){
	printf("%s\n", doc->paragraphs[i].lines[j]);
      }
      /* Used to seperate paragraphs */
      if(i < (doc->number_of_paragraphs)-1){
         printf("\n");
      }
     }
    i++;
  }
  return SUCCESS;
}



/* Adds a paragraph after the specified paragraph number.*/
int add_paragraph_after(Document *doc, int paragraph_number){
  int i = 0;
  Paragraph new_para;
  new_para.number_of_lines = 0;
  
  if(doc == NULL || paragraph_number > doc->number_of_paragraphs ||
     doc->number_of_paragraphs == MAX_PARAGRAPHS){
    return FAILURE;
  }

  /* If the para num is 0 it gets addes to the starting. */
  /*if(paragraph_number == 0){
    doc->paragraphs[0] = new_para;
  }*/
  /*The paragraph gets added at the specified location.*/
  for(i = doc->number_of_paragraphs; i >= paragraph_number; i--){
      doc->paragraphs[i] = doc->paragraphs[i-1];
  }
  doc->paragraphs[paragraph_number] = new_para;
  (doc->number_of_paragraphs)++;
  return SUCCESS;
}

/*Adds a new line after the specified line number*/
int add_line_after(Document *doc, int paragraph_number,
		   int line_number,
		   const char *new_line){
  int i = 0;
  char *line0, *line1, *line2, *line_new;
    
  if(doc == NULL || paragraph_number > doc->number_of_paragraphs ||
     doc->paragraphs[paragraph_number-1].number_of_lines ==
     MAX_PARAGRAPH_LINES || line_number >
     doc->paragraphs[paragraph_number-1].number_of_lines ||
     new_line == NULL){
    return FAILURE;
  }
  /*If the number is 0 it gets added to the beganing.*/
  if(line_number == 0){
    line0 = doc->paragraphs[paragraph_number-1].lines[0];
    strcpy(line0, new_line);
  }
  /* The line is added at the specified location. */
  for(i =  doc->paragraphs[paragraph_number-1].number_of_lines;
      i >= line_number; i-- ){
    line1 = doc->paragraphs[paragraph_number-1].lines[i];
    line2 = doc->paragraphs[paragraph_number-1].lines[i-1];
    strcpy(line1, line2);
  }
  line_new = doc->paragraphs[paragraph_number-1].
    lines[line_number];
  strcpy(line_new, new_line);
  doc->paragraphs[paragraph_number-1].number_of_lines++;    
  return SUCCESS;
}

/*Gets the number of lines in a specified paragraph.*/
int get_number_lines_paragraph(Document *doc,
			       int paragraph_number,
			       int *number_of_lines){
  int i = 0;
  
  if(doc == NULL || number_of_lines == NULL ||
     paragraph_number > doc->number_of_paragraphs){
    return FAILURE;
  }

  while(i < doc->paragraphs[paragraph_number].number_of_lines){
    /*Counts the lines.*/
    (*number_of_lines)++;
    i++;
  }
  return SUCCESS;
}

/*Appends a line to the specified paragraph.*/
int append_line(Document *doc, int paragraph_number,
		const char *new_line){
  int i = 0;
  char *line0;
  
  if(doc == NULL || paragraph_number > doc->number_of_paragraphs ||
     doc->paragraphs[paragraph_number-1].number_of_lines ==
     MAX_PARAGRAPH_LINES ||  new_line == NULL){
    return FAILURE;
  }
  while(i < doc->paragraphs[paragraph_number-1].number_of_lines){
      i++;
  }
  line0 = doc->paragraphs[paragraph_number-1].lines[i];
  strcpy(line0, new_line);
  (doc->paragraphs[paragraph_number-1].number_of_lines)++;
  return SUCCESS;
}

/*Removes the specified line from the specified paragraph.*/
int remove_line(Document *doc, int paragraph_number,
		int line_number){
  int i = line_number - 1;
  
  if(doc == NULL || paragraph_number > doc->number_of_paragraphs ||
     line_number > doc->paragraphs[paragraph_number-1].
     number_of_lines){
    return FAILURE;
  }
  /* deletes the line.*/ 
  while(i <= doc->paragraphs[paragraph_number-1].number_of_lines){
    strcpy(doc->paragraphs[paragraph_number-1].lines[i],
	   doc->paragraphs[paragraph_number-1].lines[i+1]);
    i++;
  }
  doc->paragraphs[paragraph_number-1].number_of_lines--;
  return SUCCESS;
}

/*Loads the document from an array.*/
/* It loads paragraphs and lines.*/
int load_document(Document *doc, char data[][MAX_STR_SIZE + 1],
		  int data_lines){
  int i = 0;
  
  if(doc == NULL || data_lines == 0 || data == NULL){
    return FAILURE;
  }

  add_paragraph_after(doc, doc->number_of_paragraphs);

  for(i = 0; i < data_lines; i++){
    if(data[i][0] == 0){
       add_paragraph_after(doc, doc->number_of_paragraphs);
    }else{
      append_line(doc, doc->number_of_paragraphs, data[i]);
    }
  }
  return SUCCESS;
}

/*Helper Method for replace_text.*/
void replaceWord(const char *s, const char *oldW,
                  const char *newW, char *result){
  int i, cnt = 0;
  int newWlen = strlen(newW);
  int oldWlen = strlen(oldW);  
 
  /* Counting the number of times old word*/
  /*occur in the string*/
  for (i = 0; s[i] != '\0'; i++) {
    if (strstr(&s[i], oldW) == &s[i]){
      cnt++;

      /* Jumping to index after the old word.*/
      i += oldWlen - 1;
      }
  }
  i = 0;  
  while (*s){
   /* compare the substring with the result*/
   if (strstr(s, oldW) == s){
     strcpy(&result[i], newW);
     i += newWlen;
     s += oldWlen;
   }else{
     result[i++] = *s++;
   }
  }  
  result[i] = '\0';
}

/* Replaces all occurence of the text in the document.*/
int replace_text(Document *doc, const char *target,
		 const char *replacement){
    char result[MAX_STR_SIZE + 1];
    int i = 0, j = 0;

    if(doc == NULL || target == NULL || replacement == NULL){
        return FAILURE;
    }
    /* Iterates through each paragraph.*/
    for(i = 0; i < doc->number_of_paragraphs; i++){
        for(j = 0; j < doc->paragraphs[i].number_of_lines;
	    j++){
            replaceWord(doc->paragraphs[i].lines[j], target,
			replacement, result);
            sprintf(doc->paragraphs[i].lines[j],"%s", result);         
        }  
    }      
  return SUCCESS;
}


/* The function highlights every occurence of the text in the */
/* entire document.*/
int highlight_text(Document *doc, const char*target){
  char res[MAX_STR_SIZE + 1] = {'\0'};
  
  if(doc == NULL || target == NULL){
    return FAILURE;
  }
  /*Adds front and end brackets.*/
  strcat(res, HIGHLIGHT_START_STR);
  strcat(res, target);
  strcat(res, HIGHLIGHT_END_STR);
  replace_text(doc, target, res);
  return SUCCESS;
}

/*The function removes all the occurrence of the specified*/
/*text in the given document.*/
int remove_text(Document *doc, const char *target){
  if(doc == NULL || target == NULL){
    return FAILURE;
  }
  replace_text(doc, target, "");
  return SUCCESS;
}

/* Loads a new file and a default paragraph is added.*/
int load_file(Document *doc, const char *filename){
  FILE *new_file = fopen(filename, "r");
  char line[MAX_STR_SIZE+1];
  int count = 0, i, para;
  
  if(doc == NULL || filename == NULL || new_file == NULL){
    return FAILURE;
  }
  para = 0;
  add_paragraph_after(doc, 0);
  para++;
  while(fgets(line, MAX_STR_SIZE+1, new_file) != NULL){
    count = 0;    
    for(i = 0; i < (int)strlen(line); i++) {
      if(!isspace(line[i])) {
        count++;
        break;
      }
    }
    line[strlen(line) - 1] = '\0';
    if(count != 0){
      append_line(doc, para, line);
    }else{
      add_paragraph_after(doc, para);
      para++;
    }
  }
  return SUCCESS;
}

/* Prints the paragraph that is in the document of the */
/* specified file.*/
int save_document(Document *doc, const char *filename){
  FILE *new_file = fopen(filename, "w");
  int i = 0, j = 0;
  
  if(doc == NULL || filename == NULL || new_file == NULL){
    return FAILURE;
  }

  
  for(i = 0; i < doc->number_of_paragraphs; i++) {
      for(j = 0; j < doc->paragraphs[i].number_of_lines; j++) {
	fprintf(new_file, "%s\n", doc->paragraphs[i].lines[j]);
	/*printf("%s\n", doc->paragraphs[i].lines[j]);*/
	 
      }
      if(i != doc->number_of_paragraphs - 1) {
      fprintf(new_file,"\n");
      
      }       
  }
  return SUCCESS;
}
    
